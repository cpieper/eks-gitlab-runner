# GitLab Multi-Arch Runner Demo

This CDK project creates a GitLab multi-architecture runner environment using
Amazon EKS. It is intended to accompany the upcoming blog post describing the
solution.

## Prerequisites

* NodeJS (tested with 16.15.0)
* npm (tested with 8.5.5)

## Required secrets

In order to connect to the GitLab API, you must create some Secrets
in your EKS cluster.

```sh
# These two values must match the parameters supplied to the `GitLabRunner` constructor
NAMESPACE=gitlab
SECRET_NAME=gitlab-runner-secret
# The value of the registration token.
TOKEN=GRxxxxxxxxxxxxxxxxxxxxxx

kubectl -n $NAMESPACE create secret generic $SECRET_NAME \
        --from-literal="runner-registration-token=$TOKEN" \
        --from-literal="runner-token="
```

## Building the stack

```shell
npm install
npm run build
npx cdk deploy
```

