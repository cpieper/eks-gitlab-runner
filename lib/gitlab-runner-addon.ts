import { HelmAddOn, ClusterInfo, Values } from '@aws-quickstart/eks-blueprints';
import { Construct } from 'constructs';
import { uniq } from 'lodash';

const CHART_REPO = 'https://charts.gitlab.io';
const HELM_CHART = 'gitlab-runner';
const DEFAULT_NAMESPACE = 'gitlab';
const DEFAULT_VERSION = '0.40.1';

export enum CpuArch {
    ARM_64 = 'arm64',
    X86_64 = 'amd64'
}

interface GitLabRunnerProps {
    arch: CpuArch
    gitlabUrl: string
    secretName: string
    tags?: string[]
    namespace?: string
    chartVersion?: string
}

export class GitLabRunner extends HelmAddOn {
    private arch: CpuArch;
    private gitlabUrl: string;
    private secretName: string;
    private tags: string[] = [];

    constructor(props: GitLabRunnerProps) {
        super({
            name: `gitlab-runner-${props.arch}`,
            chart: HELM_CHART,
            repository: CHART_REPO,
            namespace: props.namespace || DEFAULT_NAMESPACE,
            version: props.chartVersion || DEFAULT_VERSION,
            release: `gitlab-runner-${props.arch}`,
        });

        this.arch = props.arch;
        this.gitlabUrl = props.gitlabUrl;
        this.secretName = props.secretName;

        switch (this.arch) {
            case CpuArch.X86_64:
                this.tags.push('amd64', 'x86', 'x86-64', 'x86_64');
                break;
            case CpuArch.ARM_64:
                this.tags.push('arm64');
                break;
        }
        this.tags.push(...props.tags || []);
    };

    deploy(clusterInfo: ClusterInfo): void | Promise<Construct> {
        const chart = this.addHelmChart(clusterInfo, this.getValues(), true);
        return Promise.resolve(chart);
    }

    private getValues(): Values {
        return {
            gitlabUrl: this.gitlabUrl,
            runners: {
                // For the executor
                config: this.runnerConfig(),
                name: `demo-runner-${this.arch}`,
                tags: uniq(this.tags).join(','),
                secret: this.secretName,
            },
            // For the runner
            nodeSelector: {
                'kubernetes.io/arch': this.arch,
                'karpenter.sh/capacity-type': 'on-demand'
            },
            podLabels: {
                'gitlab-role': 'manager'
            },
            rbac: {
                create: true
            },
            resources: {
                requests: {
                    memory: '128Mi',
                    cpu: '256m'
                }
            }
        };
    }

    private runnerConfig(): string {
        return `
  [[runners]]
    [runners.kubernetes]
      namespace = "{{.Release.Namespace}}"
      image = "ubuntu:16.04"
      cpu_request = "1"
      cpu_request_overwrite_max_allowed = "2"
      memory_request = "256Mi"
      memory_request_overwrite_max_allowed = "4Gi"
    [runners.kubernetes.node_selector]
      "kubernetes.io/arch" = "${this.arch}"
      "kubernetes.io/os" = "linux"
      "karpenter.sh/capacity-type" = "spot"
    [runners.kubernetes.pod_labels]
      gitlab-role = "runner"
      `.trim();
    }
}