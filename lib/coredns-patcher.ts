import { KubernetesPatch, PatchType } from "aws-cdk-lib/aws-eks";
import { ClusterAddOn, ClusterInfo } from "@aws-quickstart/eks-blueprints";

export class CoreDNSFargatePatch implements ClusterAddOn {
    deploy(clusterInfo: ClusterInfo): void {
        const cluster = clusterInfo.cluster;
        new KubernetesPatch(cluster.stack, "RemoveComputeTypeAnnotation", {
            cluster,
            resourceNamespace: 'kube-system',
            resourceName: 'deployment/coredns',
            patchType: PatchType.JSON,
            applyPatch: [
                {
                    op: 'remove',
                    path: '/spec/template/metadata/annotations/eks.amazonaws.com~1compute-type'
                }
            ],
            restorePatch: []
        });
    }
}